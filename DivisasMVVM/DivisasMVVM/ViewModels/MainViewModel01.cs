﻿using DivisasMVVM.Services;
using GalaSoft.MvvmLight.Command;
using System.ComponentModel;
using System.Windows.Input;

namespace DivisasMVVM.ViewModels
{
    public class MainViewModel01 : INotifyPropertyChanged
    {
        #region Attributes

        private DialogService dialogService;

        private decimal pesos;

        private decimal dollars;

        private decimal pounds;

        private decimal euros;

        #endregion

        #region Properties

        public decimal Pesos
        {
            set
            {
                if (pesos != value)
                {
                    pesos = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pesos"));
                }
            }
            get
            {
                return pesos;
            }
        }

        public decimal Dollars
        {
            set
            {
                if (dollars != value)
                {
                    dollars = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Dollars"));
                }
            }
            get
            {
                return dollars;
            }
        }

        public decimal Pounds
        {
            set
            {
                if (pounds != value)
                {
                    pounds = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pounds"));
                }
            }
            get
            {
                return pounds;
            }
        }

        public decimal Euros
        {
            set
            {
                if (euros != value)
                {
                    euros = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Euros"));
                }
            }
            get
            {
                return euros;
            }
        }

        #endregion

        #region Constructors

        public MainViewModel01()
        {
            dialogService = new DialogService();
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Commands

        public ICommand ConvertCommand { get { return new RelayCommand(ConvertMoney); } }

        private async void ConvertMoney()
        {
            if (Pesos <= 0)
            {
                await dialogService.ShowMessage("Error", "Debe ingresar un valor en pesos mayor a cero (0)");
                return;
            }

            Dollars = Pesos / (decimal)2881.84438;
            Pounds = Pesos / (decimal)3608.93372;
            Euros = Pesos / (decimal)3101.61383;
        }
        #endregion
    }
}
